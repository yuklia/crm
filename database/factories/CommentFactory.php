<?php

use Faker\Generator as Faker;

$factory->define(\App\Comment::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'crmId' => $faker->uuid,
        'createdTime' => $faker->dateTime(),
        'content' => $faker->text()
    ];
});
