<?php

namespace App\Providers;

use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;
use App\Services\Panda\API as PandaAPI;

class PandaServiceProvider extends ServiceProvider
{
    protected $defer = true;

    protected $host = 'https://nlmarkets.pandats-api.io';

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(PandaAPI::class, function ($app) {
            $client = new Client(['base_uri' => $this->host]);
            return new PandaAPI($client);
        });
    }

    public function provides()
    {
        return [PandaAPI::class];
    }
}
