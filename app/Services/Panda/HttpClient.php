<?php

namespace App\Services\Panda;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\UriInterface;

/**
 * Class HttpClient
 * @package App\Services\Panda
 * @method array get(string|UriInterface $uri, array $options = [])
 * @method array post(string|UriInterface $uri, array $options = [])
 */
class HttpClient
{
    protected $jwtToken = '';

    /** @var GuzzleClient  */
    protected $client;

    public function __construct(GuzzleClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param $method
     * @param $arguments
     * @return array
     */
    public function __call($method, $arguments) {

        if (!in_array($method, ['post', 'get'])) {
            throw new \BadMethodCallException(
                sprintf('Call to undefined method %s->%s()',
                get_class($this),
                $method)
            );
        }
        $responseData = [];

        try{
            $response = call_user_func_array(array(&$this->client, $method), $arguments);
            $body = $response->getBody();
            $content = json_decode($body->getContents());
            $responseData = $content->data;
        } catch(ClientException $clientException) {
            Log::error($clientException->getMessage());
        }

        return $responseData;
    }

    private function generateAccessToken(): string
    {
        $partnerId = config('services.panda.partner_id');
        $secretKey = config('services.panda.key');
        $time = time();
        $concatenatedString = $partnerId . $time . $secretKey;
        $accessKey = sha1($concatenatedString);
        return $accessKey;
    }

    private function getJWT()
    {
        $endpoint = '/api/v3/authorization';
        $responseData = $this->post($endpoint, [
            'form_params' => [
                'partnerId' => config('services.panda.partner_id'),
                'time' => time(),
                'accessKey' => $this->generateAccessToken()
            ]
        ]);

        return $responseData->token;
    }

    public function getToken()
    {
        if ($this->jwtToken == '') {
            $this->jwtToken = $this->getJWT();
        }
        return $this->jwtToken;
    }
}
