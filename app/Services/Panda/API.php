<?php

namespace App\Services\Panda;

class API extends HttpClient
{
    public function getComments() : array
    {
        $endpoint = '/api/v3/customers/comments';
        $responseData = $this->get($endpoint, [
            'headers' => [
                'Authorization' => 'Bearer'. ' '. $this->getToken()
            ]
        ]);

        return $responseData;
    }
}
