@extends('layouts.app')
@section('content')
    <div class="container">

        <div class="alert alert-info" role="alert" v-if="comments.length == 0">
            No comments yet.
        </div>

        <table class="table table-bordered" v-else>
            <tr>
                <th>Email</th>
                <th>CRMId</th>
                <th>CreateTime</th>
                <th>Content</th>
            </tr>
            <tr v-for="comment in comments">
                <td>@{{ comment.email }}</td>
                <td>@{{ comment.crmId }}</td>
                <td>@{{ comment.createdTime }}</td>
                <td>@{{ comment.content }}</td>
            </tr>
        </table>
        <vue-pagination  v-bind:pagination="pagination"
                         v-on:click.native="getComments(pagination.current_page)"
                         :offset="4">
        </vue-pagination>
    </div>
@endsection