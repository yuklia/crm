<?php

namespace App\Http\Controllers\Api\V1;

use App\Comment;
use App\Http\Controllers\Controller;
use App\Services\Panda\API as PandaAPI;

class CommentsController extends Controller
{
    public function index(PandaAPI $api)
    {
        $rowsPerPage = 5;
        $comments = Comment::all();
        if ($comments->isEmpty()) {
            $comments = $api->getComments();
            if (!empty($comments)) {
                Comment::insert($comments);
            }
        }

        $comments = Comment::paginate($rowsPerPage);
        return response()->json($comments);
    }
}
