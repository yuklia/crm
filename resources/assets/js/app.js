require('./bootstrap');

window.Vue = require('vue');

import axios from 'axios';

Vue.component('vue-pagination', require('./components/Pagination.vue'));

const  app = new Vue({
    el: '#app',
    data: {
        comments: [],
        counter: 0,
        pagination: {
            total: 0,
            per_page: 2,
            from: 1,
            to: 0,
            current_page: 1
        },
        offset: 4,
    },
    mounted : function() {
        this.getComments(this.pagination.current_page);
    },
    methods: {
        getComments(page) {
            let _this = this;
            axios.get(`/api/v1/comments?page=${page}`).then(response => {
                _this.comments = response.data.data;
                _this.pagination = response.data;
            });
        }
    }
});
